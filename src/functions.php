<?php
    function sendMessage(string $nickname, string $message)
    {
        echo "[" . date('Y-m-d H:i:s') . "] $nickname:" . strtoupper($message) . PHP_EOL;
    }
    sendMessage('vinicius','greetings');
    sendMessage('alves', 'thanks');

    function multCalc(int $x, int $y)
    {
        echo $x * $y . PHP_EOL;
    }
    multCalc(2, 5);

    function divCalc(int $x, int $y): int
    {
        $res = $x / $y;
        // echo $res . PHP_EOL;
        return $res;
    }
    $result = divCalc(10, 5);
    echo $result . PHP_EOL;
?>
