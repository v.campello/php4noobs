<?php

$employee = true;
$homeOffice = true;
var_dump($employee && $homeOffice );

$employee = false;
$homeOffice = false;
var_dump($employee && $homeOffice );

$employee = true;
$homeOffice = false;
var_dump($employee && $homeOffice );

$employee = true;
$homeOffice = false;
var_dump($employee || $homeOffice );

$employee = false;
$homeOffice = false;
var_dump($employee || $homeOffice );

$homeOffice = true;
var_dump(!$homeOffice );

$homeOffice = false;
var_dump(!$homeOffice );
?>
