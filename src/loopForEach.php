<?php
$user = [
    'name' => 'vinicius',
    'age' => 29,
    'employed' => true
];
foreach ($user as $fields) {
    echo $fields . PHP_EOL;
}

$user = [
    'name' => 'vinicius',
    'age' => 29,
    'employed' => true
];
foreach ($user as $key => $value) {
    echo $key . " " . $value . PHP_EOL;
}

$user = [[
    'name' => 'vinicius',
    'age' => 29,
    'employed' => true
],
[
    'name' => 'alves',
    'age' => 29,
    'employed' => true
]];
foreach ($user as $key => $value) {
    echo $key . " " . $value['age'] . PHP_EOL;
}

$user = new StdClass; /**instacia de classe */
$user->name = 'vinicius';
$user->age = 29;
foreach($user as $key => $value) {
    echo $key . " " . $value . PHP_EOL;
}

$names = ['a', 'f', 'ç'];
foreach($names as $key => $value) {
    echo $key . " " . $value . PHP_EOL;
}

?>