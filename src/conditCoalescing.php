<?php

$user = [
    'name' => 'vinicius',
    'age' => 29,
    'twitch' => 'partner'
];
echo $user['name'] ?? $user['twitch'] ?? 'ultima string' . PHP_EOL;
echo isset($user['name']) ? $user['name'] : 'nome incorreto' . PHP_EOL;

$user = [
    'age' => 29
];
echo $user['name'] ?? $user['twitch'] ?? 'retorno default da condição' . PHP_EOL;
echo isset($user['name']) ? $user['name'] : 'nome incorreto' . PHP_EOL;
echo $user['sub'] ?? 'não possui a propriedade sub' . PHP_EOL;

?>