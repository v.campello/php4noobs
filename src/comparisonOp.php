<?php

$expressionA = (1 == 1) ;
var_dump($expressionA);

$expressionA = (1 == 2) ;
var_dump($expressionA);

$expressionA = 'example' ;
var_dump($expressionA == 'example');

$expressionA = 'example' ;
var_dump($expressionA == 'name  ');

$expressionA = 4 ;
var_dump($expressionA > 9);

$expressionA = 4 ;
var_dump($expressionA >= 4);
?>