<?php

$itemA = 10;
$itemB = 20;
$itemC = ($itemA + $itemB + 10) + 10;
echo $itemC . PHP_EOL;

$itemA = 10;
$itemB = 20;
$itemC = ($itemA - $itemB + 10) + 10;
echo $itemC . PHP_EOL;

$itemC = $itemA / $itemB;
echo $itemC . PHP_EOL;

$itemC = $itemA * $itemB;
echo $itemC . PHP_EOL;

$itemA = 10;
$itemB = 20;
$itemC = $itemA % $itemB;
echo $itemC . PHP_EOL;

?>